import { validate, ValidationError } from '@upe/ngx-form-validators';
import { DateFormControl, NumberFormControl, TextFormControl } from '../../src/controls/index';
import { SelectFormControl } from '../../src/controls/select.form-control';
import { Group } from '../../src/generic-form/decoretors/group';
import {
  Control, Default, DisabledIf, LabelIf, Max, Min, Options, Required, Validator,
} from '../../src/generic-form/decoretors/index';
import { GenericFormGroup } from '../../src/generic-form/index';

@Group()
export class AnstellungsformForm extends GenericFormGroup<any> {

  @Control() @Required() @Options('WiMi',
                                  'HiWi',
                                  'BTW',
  ) @Default('WiMi') public beschaeftigungsart: SelectFormControl = undefined as any;

  @Control() @LabelIf(function (beschaeftigungsart) {
    return beschaeftigungsart.value === 'HiWi' ? 'Hiwi-Kategorie' : 'Entgeltgrupppe';
  }, 'beschaeftigungsart') @Validator(function (beschaeftigungsart) {
    if (beschaeftigungsart.value === 'HiWi') {
      validate(this).to.have.pattern(/(SHK|WHF|WHK)/);
    } else {
      validate(this).to.have.pattern(/TV-L\s\d+\w?/);
    }
  }, 'beschaeftigungsart') @DisabledIf(function (beschaeftigungsart) {
    if (beschaeftigungsart.valid) {
      throw new ValidationError('');
    }
  }, 'beschaeftigungsart') public entgeltgrouppe: TextFormControl = undefined as any;

  @Control() @Min(1) @Max(6) @DisabledIf(function (beschaeftigungsart) {
    validate(beschaeftigungsart).is.eq('HiWi');
  }, 'beschaeftigungsart') public entgeltstufe: NumberFormControl = undefined as any;

  @Control() @DisabledIf(function (beschaeftigungsart) {
    validate(beschaeftigungsart).is.eq('HiWi');
  }, 'beschaeftigungsart') public stufenerhöhung: DateFormControl = undefined as any;

  @Control() public anstellungsbeginn: DateFormControl = undefined as any;

  @Control() @Validator(function (anstellungsbeginn: DateFormControl) {
    validate(this, 'Das Datum des Anstellungsende muss nach dem Anstellungsbeginn liegen').to.be.date.and
      .after(anstellungsbeginn);
  }, 'anstellungsbeginn') public anstellungsende: DateFormControl = undefined as any;

  @Control() @Min(0) public gehalt: NumberFormControl = undefined as any;

}
