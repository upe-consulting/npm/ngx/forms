import { Component, Input, OnInit } from '@angular/core';
import { KostenstelleForm } from './kostenstelle.form';

@Component({
             selector: 'kostenstelle', template: `
    <form [formGroup]="form" *ngIf="form && form.initialized">
      <input formControlName="kostenkategorien" type="text">
      <input formControlName="verbuchungsbeginn" type="date">
      <input formControlName="verbuchungsende" type="date">
      <input formControlName="wochenstunden" type="number">
      <input formControlName="beschäftigungsumfang" type="number">
      <input formControlName="bezeichnung" type="text">
    </form>
                       `,
           })
export class KostenstelleComponent implements OnInit {

  @Input() public form: KostenstelleForm;

  public ngOnInit(): void {
    if (!this.form) {
      throw new Error('KostenstelleForm is not set');
    }
    if (!this.form.initialized) {
      throw new Error('KostenstelleForm is not initialized');
    }
  }
}
