import { DateFormControl, NumberFormControl } from '../../src/controls';
import { SelectFormControl } from '../../src/controls/select.form-control';
import { GenericFormGroup } from '../../src/generic-form';
import { Control, Group, MultiSubGroup, Options } from '../../src/generic-form/decoretors';
import { KostenstelleForm } from './kostenstelle.form';

@Group()
export class VertragForm extends GenericFormGroup<any> {

  @Control() public vertragsbeginn: DateFormControl = undefined as any;

  @Control() public vertragsende: DateFormControl = undefined as any;

  @Control() public umfang: NumberFormControl = undefined as any;

  @Control() @Options('Abgelaufen',
                      'Änderungsvertrag',
                      'Aktiv',
                      'Planung',
  ) public vertragsStatus: SelectFormControl = undefined as any;

  @MultiSubGroup(KostenstelleForm) public kostenstellen: KostenstelleForm[] = [];

}
