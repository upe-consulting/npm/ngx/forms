import { Component, Input, OnInit } from '@angular/core';
import { VertragForm } from './vertrag.form';

@Component({
             selector: 'vertrag', template: `
    <form [formGroup]="form" *ngIf="form && form.initialized">
      <input formControlName="vertragsbeginn" type="date">
      <input formControlName="vertragsende" type="date">
      <input formControlName="umfang" type="number">
      <input formControlName="vertragsStatus" type="text">
      <kostenstelle *ngFor="let kostenstelle of kostenstellen"
                    [form]="kostenstelle"></kostenstelle>
    </form>
                       `,
           })
export class VertragComponent implements OnInit {

  @Input() public form: VertragForm;

  public ngOnInit(): void {
    if (!this.form) {
      throw new Error('VertragForm is not set');
    }
    if (!this.form.initialized) {
      throw new Error('VertragForm is not initialized');
    }
  }
}
