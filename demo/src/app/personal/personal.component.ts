import { Component, OnInit } from '@angular/core';
import { PersonForm } from './person.form';

@Component({
             template: `
                         <form [formGroup]="form" (submit)="form.submit()" *ngIf="form && form.initialized">
                           <input formControlName="nachname" type="text">
                           <input formControlName="vorname" type="text">
                           <input formControlName="personalnummer" type="text">
                           <input formControlName="nameskuerzel" type="text">
                           <input formControlName="stellenumfang" type="number">
                           <input formControlName="geburtsdatum" type="date">
                           <input formControlName="unbefristet" type="checkbox">
                           <input formControlName="beschaeftigungsende" type="date">
                           <input formControlName="beschaeftigungsbeginn" type="date">
                           <anstellungsform *ngFor="let anstellungsform of form.anstellungsformen"
                                            [id]="anstellungsform.name"
                                            [form]="anstellungsform"></anstellungsform>
                           <vertrag *ngFor="let vertrag of form.vertraege"
                                    [id]="vertrag.name"
                                    [form]="vertrag"></vertrag>
                           <input type="submit">
                         </form>
                       `, providers: [PersonForm],
           })
export class PersonalComponent implements OnInit {

  constructor(public form: PersonForm) {
  }


  public ngOnInit(): void {
    this.form.init();
  }
}
