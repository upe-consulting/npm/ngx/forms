import { DateFormControl, NumberFormControl, TextFormControl } from '../../src/controls';
import { GenericFormGroup } from '../../src/generic-form';
import { Control, Group, Placeholder, Readonly } from '../../src/generic-form/decoretors';

@Group()
export class KostenstelleForm extends GenericFormGroup<any> {

  @Control() @Readonly public kostenkategorien: TextFormControl = undefined as any;

  @Control() @Placeholder('Startzeitpunkt der Verbuchung über diese Kostenstelle') public verbuchungsbeginn: DateFormControl = undefined as any;

  @Control() @Placeholder('Entzeitpunkt der Verbuchung über eine Kostenstelle') public verbuchungsende: DateFormControl = undefined as any;

  @Control() public wochenstunden: NumberFormControl = undefined as any;

  @Control() public beschäftigungsumfang: NumberFormControl = undefined as any;

  @Control() public bezeichnung: TextFormControl = undefined as any;

}
