import { Component, Input, OnInit } from '@angular/core';
import { AnstellungsformForm } from './anstellungsform.form';

@Component({
             selector: 'anstellungsform', template: `
    <form [formGroup]="form" *ngIf="form && form.initialized">
      <input formControlName="beschaeftigungsart" type="text">
      <input formControlName="entgeltgrouppe" type="text">
      <input formControlName="entgeltstufe" type="text">
      <input formControlName="stufenerhöhung" type="date">
      <input formControlName="anstellungsbeginn" type="date">
      <input formControlName="anstellungsende" type="date">
      <input formControlName="gehalt" type="number">
    </form>
                       `,
           })
export class AnstellungsformComponent implements OnInit {

  @Input() public form: AnstellungsformForm;

  public ngOnInit(): void {
    if (!this.form) {
      throw new Error('AnstellungsformForm is not set');
    }
    if (!this.form.initialized) {
      throw new Error('AnstellungsformForm is not initialized');
    }
  }
}
