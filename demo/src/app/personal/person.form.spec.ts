// tslint:disable:no-unused-expression

import { TestBed } from '@angular/core/testing';
import { expect } from 'chai';
import { checkModel, setModelToForm } from '../../../../src/tests/utils.spec';
import { PersonForm } from '../../demo/personal/person.form';

xdescribe('Personal', () => {

  describe('Form Group', () => {

    describe('Person', () => {

      let form: PersonForm;

      beforeEach(() => {
        TestBed.configureTestingModule({ providers: [PersonForm] });
        form = TestBed.get(PersonForm).init();
      });

      it('Inital invalid', () => {

        expect(form.initialized).to.be.true;

        form.validate();

        expect(form.nachname.invalid).to.be.true;

        expect(form.invalid).to.be.true;

      });

      it('Minimal valid', () => {

        form.nachname.setValue('nachname');

        form.validate();

        expect(form.nachname.valid).to.be.true;

        expect(form.valid).to.be.true;

      });

      it('with one anstellungsform', () => {

        form.createMultiSubGroup('anstellungsformen');
        const model = {
          nachname:              'nachname',
          vorname:               'vorname',
          personalnummer:        'personalnummer',
          stellenumfang:         50,
          geburtsdatum:          '1997-01-01',
          beschaeftigungsende:   '2012-01-01',
          beschaeftigungsbeginn: '2010-01-01',
          anstellungsformen:     [
            {
              beschaeftigungsart: 'WiMi',
              entgeltgrouppe:     'TV-L 23ü',
              entgeltstufe:       5,
              stufenerhöhung:     '2018-01-01',
              anstellungsbeginn:  '2014-01-01',
              anstellungsende:    '2016-01-01',
              gehalt:             100000000,
            },
          ],
        };

        setModelToForm(model, form);

        checkModel(model, form);

      });

      it('with two anstellungsform', () => {

        form.createMultiSubGroup('anstellungsformen');
        form.createMultiSubGroup('anstellungsformen');
        const model = {
          nachname:              'nachname',
          vorname:               'vorname',
          personalnummer:        'personalnummer',
          stellenumfang:         50,
          geburtsdatum:          '1997-01-01',
          beschaeftigungsende:   '2012-01-01',
          beschaeftigungsbeginn: '2010-01-01',
          anstellungsformen:     [
            {
              beschaeftigungsart: 'WiMi',
              entgeltgrouppe:     'TV-L 23ü',
              entgeltstufe:       5,
              stufenerhöhung:     '2018-01-01',
              anstellungsbeginn:  '2014-01-01',
              anstellungsende:    '2016-01-01',
              gehalt:             100000000,
            },
            {
              beschaeftigungsart: 'WiMi',
              entgeltgrouppe:     'TV-L 23ü',
              entgeltstufe:       5,
              stufenerhöhung:     '2018-01-01',
              anstellungsbeginn:  '2014-01-01',
              anstellungsende:    '2016-01-01',
              gehalt:             100000000,
            },
          ],
        };

        setModelToForm(model, form);

        checkModel(model, form);

      });

      it('with one vertrag', () => {

        form.createMultiSubGroup('vertraege');
        const model = {
          nachname:              'nachname',
          vorname:               'vorname',
          personalnummer:        'personalnummer',
          stellenumfang:         50,
          geburtsdatum:          '1997-01-01',
          beschaeftigungsende:   '2012-01-01',
          beschaeftigungsbeginn: '2010-01-01',
          vertraege:             [
            {
              vertragsbeginn: '2016-01-01',
              vertragsende:   '2018-01-01',
              umfang:         5,
              vertragsStatus: 'Abgelaufen',
            },
          ],
        };

        setModelToForm(model, form);

        checkModel(model, form);

      });

      it('with two vertrag', () => {

        form.createMultiSubGroup('vertraege');
        form.createMultiSubGroup('vertraege');
        const model = {
          nachname:              'nachname',
          vorname:               'vorname',
          personalnummer:        'personalnummer',
          stellenumfang:         50,
          geburtsdatum:          '1997-01-01',
          beschaeftigungsende:   '2012-01-01',
          beschaeftigungsbeginn: '2010-01-01',
          vertraege:             [
            {
              vertragsbeginn: '2016-01-01',
              vertragsende:   '2018-01-01',
              umfang:         5,
              vertragsStatus: 'Abgelaufen',
            },
            {
              vertragsbeginn: '2016-01-01',
              vertragsende:   '2018-01-01',
              umfang:         5,
              vertragsStatus: 'Abgelaufen',
            },
          ],
        };

        setModelToForm(model, form);

        checkModel(model, form);

      });

    });

  });

});
