import { CommonModule } from '@angular/common';
import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { async } from '../../../../src/tests/utils.spec';
import { AnstellungsformComponent } from '../../demo/personal/anstellungsform.component';
import { AnstellungsformForm } from '../../demo/personal/anstellungsform.form';
import { PersonalComponent } from '../../demo/personal/personal.component';

xdescribe('Personal', () => {

  describe('Component', () => {

    describe('Anstellungsform', () => {

      @Component({
        template: '<anstellungsform *ngIf="form" [form]="form"></anstellungsform>',
      })
      class WrapperComponent {
        public form: AnstellungsformForm | null = null;
      }

      let fixture: ComponentFixture<PersonalComponent>;
      let component: PersonalComponent;
      let debugElm: DebugElement;

      beforeEach(async(async () => {

        await TestBed.configureTestingModule({
          declarations: [
            AnstellungsformComponent,
          ],
          imports:      [
            CommonModule,
            ReactiveFormsModule,
          ],
        }).compileComponents();

        fixture   = TestBed.createComponent(PersonalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        debugElm = fixture.debugElement;

      }));


    });

  });

});
