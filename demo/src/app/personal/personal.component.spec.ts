// tslint:disable:no-unused-expression

import { CommonModule } from '@angular/common';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { expect } from 'chai';
import { A, sendModelToForm, submitAndCheckModel } from '../../../../src/tests/utils.spec';
import { AnstellungsformComponent } from '../../demo/personal/anstellungsform.component';
import { KostenstelleComponent } from '../../demo/personal/kostenstelle.component';
import { PersonalComponent } from '../../demo/personal/personal.component';
import { VertragComponent } from '../../demo/personal/vertrag.component';

xdescribe('Personal', () => {

  describe('Component', () => {

    describe('Personal', () => {

      let fixture: ComponentFixture<PersonalComponent>;
      let component: PersonalComponent;
      let debugElm: DebugElement;
      let submitInput: DebugElement;
      let nachnameInput: DebugElement;
      let vornameInput: DebugElement;
      let personalnummerInput: DebugElement;
      let nameskuerzelInput: DebugElement;
      let stellenumfangInput: DebugElement;
      let geburtsdatumInput: DebugElement;
      let unbefristetInput: DebugElement;
      let beschaeftigungsendeInput: DebugElement;
      let beschaeftigungsbeginnInput: DebugElement;

      beforeEach(A(async () => {

        await TestBed.configureTestingModule({
          declarations: [
            AnstellungsformComponent,
            VertragComponent,
            PersonalComponent,
            KostenstelleComponent,
          ],
          imports:      [
            CommonModule,
            ReactiveFormsModule,
          ],
        }).compileComponents();

        fixture   = TestBed.createComponent(PersonalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        debugElm                   = fixture.debugElement;
        submitInput                = debugElm.query(By.css('input[type=submit]'));
        nachnameInput              = debugElm.query(By.css('input[formControlName=nachname]'));
        vornameInput               = debugElm.query(By.css('input[formControlName=vorname]'));
        personalnummerInput        = debugElm.query(By.css('input[formControlName=personalnummer]'));
        nameskuerzelInput          = debugElm.query(By.css('input[formControlName=nameskuerzel]'));
        stellenumfangInput         = debugElm.query(By.css('input[formControlName=stellenumfang]'));
        geburtsdatumInput          = debugElm.query(By.css('input[formControlName=geburtsdatum]'));
        unbefristetInput           = debugElm.query(By.css('input[formControlName=unbefristet]'));
        beschaeftigungsendeInput   = debugElm.query(By.css('input[formControlName=beschaeftigungsende]'));
        beschaeftigungsbeginnInput = debugElm.query(By.css('input[formControlName=beschaeftigungsbeginn]'));

      }));

      function submit(): void {
        submitInput.triggerEventHandler('click', null);
      }

      it('without multi subgroups', () => {
        const model = {
          nachname:              'nachname',
          vorname:               'vorname',
          personalnummer:        'personalnummer',
          stellenumfang:         50,
          geburtsdatum:          '1997-01-01',
          beschaeftigungsende:   '2012-01-01',
          beschaeftigungsbeginn: '2010-01-01',
        };

        sendModelToForm(model, fixture);

        submitAndCheckModel(model, submit, fixture);
      });

      it('create anstellungsformen multi subgroups', () => {

        for (let i = 0; i < 3; i++) {

          component.form.createMultiSubGroup('anstellungsformen');

          fixture.detectChanges();

          expect(debugElm.query(By.css(`#anstellungsformen-${i}`))).to.be.not.null;

          expect(
            component.form.anstellungsformen,
            'after calling createMultiSubGroup new AnstellungsformForm should be created',
          ).have.length(i + 1);

        }

      });


      it('create verträge multi subgroups', () => {

        for (let i = 0; i < 10; i++) {

          component.form.createMultiSubGroup('vertraege');

          fixture.detectChanges();

          expect(debugElm.query(By.css(`#vertraege-${i}`))).to.be.not.null;

          expect(component.form.vertraege).have.length(i + 1);

        }

      });

      xit('with one anstellungsform', () => {
      });

    });

  });

});
