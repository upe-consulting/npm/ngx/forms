import { validate } from '@upe/ngx-form-validators';
import { CheckBoxFormControl, DateFormControl, NumberFormControl, TextFormControl } from '../../src/controls/index';
import { MultiSubGroup } from '../../src/generic-form/decoretors';
import { Control } from '../../src/generic-form/decoretors/control';
import { Group } from '../../src/generic-form/decoretors/group';
import { GenericFormGroup } from '../../src/generic-form/index';
import { DisabledIf, Max, Min, OnChange, Readonly, Required, Submit, Validator } from '../../src/public_api';
import { AnstellungsformForm } from './anstellungsform.form';
import { VertragForm } from './vertrag.form';

@Group()
export class PersonForm extends GenericFormGroup<any> {

  @Control() @Required() @Max(50) public nachname: TextFormControl = undefined as any;

  @Control() @Max(50) public vorname: TextFormControl = undefined as any;

  @Control() @Max(20) public personalnummer: TextFormControl = undefined as any;

  @Control() @Max(100) @Min(0) public stellenumfang: NumberFormControl = undefined as any;

  @Control() public beschaeftigungsbeginn: DateFormControl = undefined as any;

  @Control() public unbefristet: CheckBoxFormControl = undefined as any;

  @Control() @Validator(function (beschaeftigungsbeginn: DateFormControl) {
    validate(this, 'Das Datum des Beschäftigungsende muss nach dem Beschäftigungsbegin liegen').to.be.date.and
      .after(beschaeftigungsbeginn);
  }, 'beschaeftigungsbeginn') @DisabledIf(function (unbefristet) {
    validate(unbefristet).is.checked;
  }, 'unbefristet') public beschaeftigungsende: DateFormControl = undefined as any;

  @Control() @Validator(function () {
    validate(this, 'Das Geburtsdatum muss in der Vergangenheit liegen').to.be.date.and.before(new Date(Date.now()));
  }) public geburtsdatum: DateFormControl = undefined as any;

  @Control() @Readonly public nameskuerzel: TextFormControl = undefined as any;

  @MultiSubGroup(VertragForm) public vertraege: VertragForm[] = [];

  @MultiSubGroup(AnstellungsformForm) public anstellungsformen: AnstellungsformForm[] = [];

  @OnChange('nachname') @OnChange('vorname')
  public buildNamesKuerzel() {
    if (this.vorname.value && this.nachname.value) {
      this.nameskuerzel.setValue(this.nachname.value.substring(0, 3).toUpperCase() + this.vorname.value.substring(0, 2)
        .toUpperCase());
    }
  }

  @Submit
  public onSubmit() {
    // TODO : refactor api
    alert(JSON.stringify(this.model));
  }

}

