// tslint:disable:no-unused-expression

import { TestBed } from '@angular/core/testing';
import { expect } from 'chai';
import { AnstellungsformForm } from '../../demo/personal/anstellungsform.form';

describe('Personal', () => {

  describe('Form Group', () => {

    describe('Anstellungsform', () => {

      let form: AnstellungsformForm;

      beforeEach(() => {
        TestBed.configureTestingModule({ providers: [AnstellungsformForm] });
        form = TestBed.get(AnstellungsformForm).init();
      });

      it('WiMi', () => {

        form.beschaeftigungsart.setValue('WiMi');

        expect(form.entgeltstufe.isDisabled, 'if WiMi is selected entgelstufe should **not** be disabled').to.be.false;
        expect(
          form.stufenerhöhung.isDisabled,
          'if WiMi is selected stufenerhöhung should **not** be disabled',
        ).to.be.false;

        form.entgeltgrouppe.setValue('SHK');
        form.entgeltgrouppe.validate();

        expect(form.entgeltgrouppe.invalid, 'SHK can not used with WiMi').to.be.true;

        form.entgeltgrouppe.setValue('TV-L 13ü');
        form.entgeltgrouppe.validate();

        expect(form.entgeltgrouppe.valid, 'TV-L 13ü is a valid input').to.be.true;

      });

      it('BTW', () => {

        form.beschaeftigungsart.setValue('BTW');

        expect(form.entgeltstufe.isDisabled, 'if BTW is selected entgelstufe should **not** be disabled').to.be.false;
        expect(
          form.stufenerhöhung.isDisabled,
          'if BTW is selected stufenerhöhung should **not** be disabled',
        ).to.be.false;

        form.entgeltgrouppe.setValue('SHK');
        form.entgeltgrouppe.validate();

        expect(form.entgeltgrouppe.invalid, 'SHK can not used with BTW').to.be.true;

        form.entgeltgrouppe.setValue('TV-L 13ü');
        form.entgeltgrouppe.validate();

        expect(form.entgeltgrouppe.valid, 'TV-L 13ü is a valid input').to.be.true;

      });

      it('HiWi', () => {

        form.beschaeftigungsart.setValue('HiWi');

        expect(form.entgeltstufe.isDisabled, 'if HiWi is selected entgelstufe should be disabled').to.be.true;
        expect(form.stufenerhöhung.isDisabled, 'if HiWi is selected stufenerhöhung should be disabled').to.be.true;


        form.entgeltgrouppe.setValue('TV-L 13ü');
        form.entgeltgrouppe.validate();

        expect(form.entgeltgrouppe.invalid).to.be.true;

        form.entgeltgrouppe.setValue('SHK');
        form.entgeltgrouppe.validate();

        expect(form.entgeltgrouppe.valid).to.be.true;

      });


    });

  });

});
