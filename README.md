# NGX Forms
[![pipeline status](https://gitlab.com/upe-consulting/npm/ngx/forms/badges/master/pipeline.svg)](https://gitlab.com/upe-consulting/npm/ngx/forms/commits/master)
[![coverage report](https://gitlab.com/upe-consulting/npm/ngx/forms/badges/master/coverage.svg)](https://gitlab.com/upe-consulting/npm/ngx/forms/commits/master)

## License
MIT
