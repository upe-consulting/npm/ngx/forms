import { Inject, Injectable, Optional } from '@angular/core';
import { PARENT_GROUP } from '../generic-form';
import { GenericFormControl } from '../generic-form/generic-form-control';
import { IGenericFormGroup } from '../generic-form/generic-form-group.interface';

@Injectable()
export class LabelFormControl<G extends IGenericFormGroup = IGenericFormGroup> extends GenericFormControl<string[], G> {

  public constructor(@Inject(PARENT_GROUP) @Optional() group: any) {
    super(group);
  }

  public getModelValue(): string[] {
    return this.value;
  }

  public setModelValue(value: string[]) {
    if (value) {
      this.setValue(value);
    }
  }

}
