import { Inject, Injectable, Optional } from '@angular/core';
import { ValidationError } from '@upe/ngx-form-validators';
import { PARENT_GROUP } from '../generic-form';
import { AddValidator } from '../generic-form/decoretors/validator';
import { IGenericFormGroup } from '../generic-form/generic-form-group.interface';
import { TextFormControl } from './text.form-control';

@Injectable()
export class PatternFormControl<G extends IGenericFormGroup = IGenericFormGroup> extends TextFormControl<G> {

  public pattern: RegExp;

  public constructor(@Inject(PARENT_GROUP) @Optional() group: any) {
    super(group);
  }

  @AddValidator
  public matchPatternValidator() {
    if (!(!this.value || this.pattern.test(this.value))) {
      throw new ValidationError(this.pattern + '');
    }
  }

}
