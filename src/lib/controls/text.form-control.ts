import { Inject, Injectable, Optional } from '@angular/core';
import { PARENT_GROUP } from '../generic-form';
import { GenericFormControl } from '../generic-form/generic-form-control';
import { IGenericFormGroup } from '../generic-form/generic-form-group.interface';

@Injectable()
export class TextFormControl<G extends IGenericFormGroup = IGenericFormGroup> extends GenericFormControl<string, G> {

  public constructor(@Inject(PARENT_GROUP) @Optional() group: any) {
    super(group);
  }

}
