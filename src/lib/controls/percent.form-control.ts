import { Inject, Injectable, Optional } from '@angular/core';
import { PARENT_GROUP } from '../generic-form';
import { IGenericFormGroup } from '../generic-form/generic-form-group.interface';
import { PatternFormControl } from './pattern.form-control';

@Injectable()
export class PercentFormControl<G extends IGenericFormGroup = IGenericFormGroup> extends PatternFormControl<G> {

  public pattern = /100$|^[0-9]{1,2}$|^[0-9]{1,2}\,[0-9]{1,2}/;

  public constructor(@Inject(PARENT_GROUP) @Optional() group: any) {
    super(group);
  }

  public getModelValue() {

    let percentageRegex = /100$|^[0-9]{1,2}$|^[0-9]{1,2}\,[0-9]{1,2}/;
    if (this.value && percentageRegex.test(this.value)) {
      let value = this.value.toString().replace(',', '.');

      if (value === '.') {
        value = '0';
      }

      return Number(value) * 100;

    } else {
      return 0;
    }
  }

  public setModelValue(value: number) {

    value /= 100;

    return value.toFixed(2).toString().replace('.', ',');

  }

}
