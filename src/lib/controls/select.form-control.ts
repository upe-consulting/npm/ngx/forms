import { Inject, Injectable, Optional } from '@angular/core';
import { PARENT_GROUP } from '../generic-form';
import { ISelectOptions } from '../generic-form/decoretors/options';
import { IGenericFormGroup } from '../generic-form/generic-form-group.interface';
import { GenericFormControl } from '../generic-form/index';

@Injectable()
export class SelectFormControl<G extends IGenericFormGroup = IGenericFormGroup> extends GenericFormControl<string, G> {

  public options: ISelectOptions[] = [];

  public constructor(@Inject(PARENT_GROUP) @Optional() group: any) {
    super(group);
  }

  public addOptions(options: (string | ISelectOptions)[]) {
    for (const opt of options) {
      if (typeof opt === 'string') {
        this.options.push({
                            value: opt, option: opt,
                          });
      } else {
        this.options.push(opt);
      }
    }
  }

  public setOptions(options: (string | ISelectOptions)[]) {
    this.options = [];
    this.addOptions(options);
  }

}
