import { Inject, Injectable, Optional } from '@angular/core';
import { PARENT_GROUP } from '../generic-form';
import { ISelectOptions } from '../generic-form/decoretors/options';
import { IGenericFormGroup } from '../generic-form/generic-form-group.interface';
import { SelectFormControl } from './select.form-control';

@Injectable()
export class SelectWithIdValueFormControl<G extends IGenericFormGroup = IGenericFormGroup>
  extends SelectFormControl<G> {

  public constructor(@Inject(PARENT_GROUP) @Optional() group: any) {
    super(group);
  }

  public getModelValue() {
    const selectedOption: ISelectOptions | null = this.options.find((option: ISelectOptions) => option.value === this.value) || null;
    if (selectedOption === null) {
      return null;
    }
    return selectedOption.option;
  }

  public setModelValue(value: string) {
    if (this.group.hasOwnProperty('id')) {
      this.setValue(this.group.value.id);
    } else {
      console.warn('Form Group has not this member "id"');
    }
  }

}
