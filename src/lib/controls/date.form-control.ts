import { Inject, Injectable, Optional } from '@angular/core';
import { validate } from '@upe/ngx-form-validators';
import { PARENT_GROUP } from '../generic-form';
import { AddValidator } from '../generic-form/decoretors/validator';
import { GenericFormControl } from '../generic-form/generic-form-control';
import { IGenericFormGroup } from '../generic-form/generic-form-group.interface';

@Injectable()
export class DateFormControl<G extends IGenericFormGroup = IGenericFormGroup> extends GenericFormControl<string, G> {

  public constructor(@Inject(PARENT_GROUP) @Optional() group: any) {
    super(group);
  }

  @AddValidator
  public isDateValidator() {
    validate(this).is.date;
  }

}
