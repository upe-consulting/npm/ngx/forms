import { Inject, Injector } from '@angular/core';
import { FormArray, FormGroup, ValidationErrors } from '@angular/forms';
import { AbstractControl } from '@angular/forms/src/model';
import { Subject } from 'rxjs/Subject';
import { CONTROL_NAME, MULTI_SUB_GROUP, SUB_GROUP } from './config';
import { extractPrototype } from './decoretors/group/untils';
import { IAddIf } from './decoretors/if';
import { GenericFormControl } from './generic-form-control';
import { IGenericFormGroup } from './generic-form-group.interface';

export abstract class GenericFormGroup<T = any, MSGT extends string = string> extends FormGroup
  implements IGenericFormGroup<T> {

  public get initialized(): boolean {
    return this._initialized;
  }

  public name: string;

  public model: T;
  public validValueChanges: Subject<T> = new Subject<T>();
  protected _initialized               = false;

  public constructor(@Inject(Injector) public readonly injector: Injector) {
    super({});
    this.valueChanges.skipWhile(() => this.invalid).subscribe(() => this.validValueChanges.next(this.value));
  }

  public init<Self>(this: Self, model?: T, readonly: boolean = false): Self {
    throw new Error('Method not implemented.');
  }

  public beforeValidate(): void {
    throw new Error('Method not implemented.');
  }

  public afterValidate(): void {
    throw new Error('Method not implemented.');
  }

  public validate(): ValidationErrors[] | ValidationErrors | null {
    throw new Error('Method not implemented.');
  }

  public beforeSubmit(): boolean {
    throw new Error('Method not implemented.');
  }

  public submit(): boolean {
    throw new Error('Method not implemented.');
  }

  public afterSubmit(): boolean {
    throw new Error('Method not implemented.');
  }

  public getControlsByNames(controlNames: string[]): GenericFormControl<any>[] {
    throw new Error('Method not implemented.');
  }

  public addIf(addIfObj: IAddIf): void {
    throw new Error('Method not implemented.');
  }

  public removeIf(removeIfObj: IAddIf): void {
    throw new Error('Method not implemented.');
  }

  public addControl(name: string, control: AbstractControl): void {
    super.addControl(name, control);
    control['isAdded'] = true;
  }

  public createMultiSubGroup(name: MSGT, model?: any): void {
    throw new Error('Method not implemented.');
  }

  public removeMultiSubGroup(name: MSGT, index: number): void {
    throw new Error('Method not implemented.');
  }

  public countMultiSubGroup(name: MSGT): number {
    throw new Error('Method not implemented.');
  }

  public setValues(model: T): void {

    const prototype = extractPrototype(this);

    const propertyKeys = Object.getOwnPropertyNames(this)
      .filter((propertyKey: string) => Reflect.hasMetadata(CONTROL_NAME, prototype, propertyKey));

    propertyKeys.filter((pk) => model[Reflect.getMetadata(CONTROL_NAME, prototype, pk)] !== undefined)
      .forEach((propertyKey) => this[propertyKey].setValue(model[Reflect.getMetadata(CONTROL_NAME,
                                                                                     prototype,
                                                                                     propertyKey,
      )]));

    propertyKeys.filter((pk) => model[Reflect.getMetadata(SUB_GROUP, prototype, pk)] !== undefined)
      .forEach((propertyKey) => this[propertyKey].setValues(model[Reflect.getMetadata(SUB_GROUP,
                                                                                      prototype,
                                                                                      propertyKey,
      )]));

    propertyKeys.filter((pk) => model[Reflect.getMetadata(MULTI_SUB_GROUP, prototype, pk)] !== undefined)
      .forEach((propertyKey) => {
        const formArray = (this[propertyKey] as FormArray);
        const controls  = Object.keys(formArray.controls);
        for (let i = 0; i < controls.length; i++) {
          formArray.removeAt(0);
        }
        const models: any[] = model[Reflect.getMetadata(MULTI_SUB_GROUP, prototype, propertyKey)];
        models.forEach((m) => this.createMultiSubGroup(propertyKey as any, m));
      });

  }

  /**
   * @deprecated use setValues
   */
  public setValue(value: {
    [key: string]: any;
  }, options?: {
    onlySelf?: boolean; emitEvent?: boolean;
  }): void {
    super.setValue(value, options);
  }

  public removeControl(name: string): void {
    if (this.controls[name]) {
      this.controls[name]['isAdded'] = false;
    } else {
      throw new Error(`This control '${name}' is not added to this group`);
    }
    super.removeControl(name);
  }

}

