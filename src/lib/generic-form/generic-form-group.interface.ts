import { FormGroup, ValidationErrors } from '@angular/forms';

export interface IGenericFormGroup<T = any> extends FormGroup {
  model: T;

  init(): IGenericFormGroup<T>;

  beforeSubmit(): boolean;

  submit(): boolean;

  afterSubmit(): boolean;

  beforeValidate(): void;

  validate(): ValidationErrors[] | ValidationErrors | null;

  afterValidate(): void;
}