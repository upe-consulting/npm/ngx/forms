// tslint:disable:no-unused-expression

import { Component, DebugElement, Inject, Injectable, Injector, OnInit, Optional } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { validate } from '@upe/ngx-form-validators';
import { expect } from 'chai';
import { CheckBoxFormControl } from '../lib/controls/check-box.form-control';
import { TextFormControl } from '../lib/controls/text.form-control';
import { PARENT_GROUP } from '../lib/generic-form';
import { Control } from '../lib/generic-form/decoretors/control';
import { Group } from '../lib/generic-form/decoretors/group';
import { AddIf, RemoveIf } from '../lib/generic-form/decoretors/if';
import { Required } from '../lib/generic-form/decoretors/required';
import { AddValidator } from '../lib/generic-form/decoretors/validator';
import { GenericFormControl } from '../lib/generic-form/generic-form-control';
import { GenericFormGroup } from '../lib/generic-form/generic-form-group';
import { A } from './utils.spec';

interface IRegistration {
  username: string;
  password: string;
  email: string;
  newsLetter: boolean;
  newsLetterEmail?: string;
}

class EmailFormControl extends GenericFormControl<string> {

  public constructor(@Inject(PARENT_GROUP) @Optional() parent: any) {
    super(parent);
  }

  @AddValidator
  public isEmailValidator() {
    validate(this).to.be.an.email;
  }

}

@Injectable()
@Group()
class RegistrationForm extends GenericFormGroup<IRegistration> {

  @Control()
  @Required()
  public username: TextFormControl = undefined as any;

  @Control()
  @Required()
  public password: TextFormControl = undefined as any;

  @Control()
  @Required()
  public email: EmailFormControl = undefined as any;

  @Control()
  public newsLetter: CheckBoxFormControl = undefined as any;

  @Control()
  @Required()
  @AddIf((newsLetter) => {
    validate(newsLetter).is.checked;
  }, 'newsLetter')
  @RemoveIf((newsLetter) => {
    validate(newsLetter).is.not.checked;
  }, 'newsLetter')
  public newsLetterEmail: EmailFormControl = undefined as any;

  constructor(@Inject(Injector) i) {
    super(i);
  }

}

describe('Generic Form', () => {

  describe('Registration Form', () => {

    describe('Form Group', () => {

      describe('With empty model', () => {

        beforeEach(() => {
          TestBed.configureTestingModule({ providers: [RegistrationForm] });
        });

        it('Create', () => {
          const form = TestBed.get(RegistrationForm);

          expect(form, 'form instance should be created').to.be.not.undefined;

          expect(form.username, 'username form control instance should be **not** created').to.be.undefined;
          expect(form.password, 'username form control instance should be **not** created').to.be.undefined;
          expect(form.email, 'username form control instance should be **not** created').to.be.undefined;
          expect(form.newsLetter, 'username form control instance should be **not** created').to.be.undefined;
          expect(form.newsLetterEmail, 'username form control instance should be **not** created').to.be.undefined;

        });

        it('Init', () => {
          const form = TestBed.get(RegistrationForm);

          try {
            form.init();
          } catch (e) {
            console.log(e);
            expect.fail('throws an exception, throws not an exception', e.message);
          }

          expect(form.initialized).to.be.true;

          expect(form.username, 'username form control instance should be created').to.be.not.undefined;
          expect(form.password, 'username form control instance should be created').to.be.not.undefined;
          expect(form.email, 'username form control instance should be created').to.be.not.undefined;
          expect(form.newsLetter, 'username form control instance should be created').to.be.not.undefined;
          expect(form.newsLetterEmail, 'username form control instance should be created').to.be.not.undefined;

          expect(form.controls).has.ownProperty('username');
          expect(form.controls).has.ownProperty('password');
          expect(form.controls).has.ownProperty('email');
          expect(form.controls).has.ownProperty('newsLetter');
          expect(form.controls).has.not.ownProperty('newsLetterEmail');

          expect(form.username.validatorFunctions).to.have.length(1);
          expect(form.password.validatorFunctions).to.have.length(1);
          expect(form.email.validatorFunctions).to.have.length(2);
          expect(form.newsLetter.validatorFunctions).to.have.length(0);
          expect(form.newsLetterEmail.validatorFunctions).to.have.length(2);

        });

        it('Invalid', () => {
          const form = TestBed.get(RegistrationForm).init();

          expect(form.validate()).to.be.not.null;

          form.updateValueAndValidity();

          expect(form.invalid).to.be.true;

        });

        it('Valid without news letter email', () => {
          const form = TestBed.get(RegistrationForm).init();

          form.username.setValue('username');
          form.password.setValue('password');
          form.email.setValue('mail@mail.de');

          expect(form.validate()).to.be.null;

          form.updateValueAndValidity();

          expect(form.valid).to.be.true;

        });

        it('news letter checkbox', () => {
          const form = TestBed.get(RegistrationForm).init();

          expect(form.controls).has.not.ownProperty('newsLetterEmail');
          form.newsLetter.setValue(true);
          expect(form.controls).has.ownProperty('newsLetterEmail');
          form.newsLetter.setValue(false);
          expect(form.controls).has.not.ownProperty('newsLetterEmail');
          form.newsLetter.setValue(true);
          expect(form.controls).has.ownProperty('newsLetterEmail');
          form.newsLetter.setValue(false);
          expect(form.controls).has.not.ownProperty('newsLetterEmail');

        });

        it('Invalid with news letter checkbox', () => {
          const form = TestBed.get(RegistrationForm).init();

          form.newsLetter.setValue(true);

          expect(form.newsLetterEmail.value).to.be.null;

          expect(form.validate()).to.be.not.null;

          form.updateValueAndValidity();

          expect(form.invalid).to.be.true;

        });

        it('Valid with news letter email', () => {
          const form = TestBed.get(RegistrationForm).init();

          form.newsLetterEmail.setValue('mail@mail.de');

          expect(form.validate()).to.be.null;

          form.updateValueAndValidity();

          expect(form.valid).to.be.true;

        });

        it('Valid without news letter email', () => {
          const form = TestBed.get(RegistrationForm).init();

          form.newsLetterEmail.setValue('');

          expect(form.validate()).to.be.not.null;

          form.updateValueAndValidity();

          expect(form.invalid).to.be.true;

        });

      });

      describe('With model', () => {

        beforeEach(() => {
          TestBed.configureTestingModule({ providers: [RegistrationForm] });
        });

        const model: IRegistration = {
          username:        'modelUsername',
          password:        'modelPassword',
          email:           'modelEmail@mail.com',
          newsLetter:      false,
          newsLetterEmail: null,
        };

        it('Create', () => {
          const form = TestBed.get(RegistrationForm);

          expect(form, 'form instance should be created').to.be.not.undefined;
          expect(form.username, 'username form control instance should be **not** created').to.be.undefined;
          expect(form.password, 'username form control instance should be **not** created').to.be.undefined;
          expect(form.email, 'username form control instance should be **not** created').to.be.undefined;
          expect(form.newsLetter, 'username form control instance should be **not** created').to.be.undefined;
          expect(form.newsLetterEmail, 'username form control instance should be **not** created').to.be.undefined;

        });

        it('Init', () => {
          const form = TestBed.get(RegistrationForm);

          try {
            form.init(JSON.parse(JSON.stringify(model)));
          } catch (e) {
            expect.fail('throws an exception, throws not an exception', e.message);
          }

          expect(form.initialized).to.be.true;

          expect(form.username, 'username form control instance should be created').to.be.not.undefined;
          expect(form.password, 'username form control instance should be created').to.be.not.undefined;
          expect(form.email, 'username form control instance should be created').to.be.not.undefined;
          expect(form.newsLetter, 'username form control instance should be created').to.be.not.undefined;
          expect(form.newsLetterEmail, 'username form control instance should be created').to.be.not.undefined;

          expect(form.controls).has.ownProperty('username');
          expect(form.controls).has.ownProperty('password');
          expect(form.controls).has.ownProperty('email');
          expect(form.controls).has.ownProperty('newsLetter');
          expect(form.controls).has.not.ownProperty('newsLetterEmail');

          expect(form.username.validatorFunctions).to.have.length(1);
          expect(form.password.validatorFunctions).to.have.length(1);
          expect(form.email.validatorFunctions).to.have.length(2);
          expect(form.newsLetter.validatorFunctions).to.have.length(0);
          expect(form.newsLetterEmail.validatorFunctions).to.have.length(2);

          expect(form.username.value).to.eq(model.username);
          expect(form.password.value).to.eq(model.password);
          expect(form.email.value).to.eq(model.email);
          expect(form.newsLetter.value).to.eq(model.newsLetter);
          expect(form.model).to.be.deep.eq(model);

          expect(form.validate()).to.be.null;

          form.updateValueAndValidity();

          expect(form.valid).to.be.true;

        });

        it('Valid without news letter email', () => {
          const form = TestBed.get(RegistrationForm).init();

          form.username.setValue('username');
          form.password.setValue('password');
          form.email.setValue('mail@mail.de');

          expect(form.validate()).to.be.null;

          form.updateValueAndValidity();

          expect(form.valid).to.be.true;

        });

        it('news letter checkbox', () => {
          const form = TestBed.get(RegistrationForm).init();

          expect(form.controls).has.not.ownProperty('newsLetterEmail');
          form.newsLetter.setValue(true);
          expect(form.controls).has.ownProperty('newsLetterEmail');
          form.newsLetter.setValue(false);
          expect(form.controls).has.not.ownProperty('newsLetterEmail');
          form.newsLetter.setValue(true);
          expect(form.controls).has.ownProperty('newsLetterEmail');
          form.newsLetter.setValue(false);
          expect(form.controls).has.not.ownProperty('newsLetterEmail');

        });

        it('Invalid with news letter checkbox', () => {
          const form = TestBed.get(RegistrationForm).init();

          form.newsLetter.setValue(true);

          expect(form.newsLetterEmail.value).to.be.null;

          expect(form.validate()).to.be.not.null;

          form.updateValueAndValidity();

          expect(form.invalid).to.be.true;

        });

        it('Valid with news letter email', () => {
          const form = TestBed.get(RegistrationForm).init();

          form.newsLetterEmail.setValue('mail@mail.de');

          expect(form.validate()).to.be.null;

          form.updateValueAndValidity();

          expect(form.valid).to.be.true;

        });

        it('Valid without news letter email', () => {
          const form = TestBed.get(RegistrationForm).init();

          form.newsLetterEmail.setValue('');

          expect(form.validate()).to.be.not.null;

          form.updateValueAndValidity();

          expect(form.invalid).to.be.true;

        });

      });

    });

    describe('Component', () => {

      @Component({
        template:  `
                     <form [formGroup]="form" (submit)="form.submit()">
                       <input formControlName="username" name="username">
                       <input formControlName="password" name="password">
                       <input formControlName="email" name="email">
                       <input formControlName="newsLetter" name="newsLetter">
                       <input *ngIf="form.controls.newsLetterEmail" name="newsLetterEmail"
                              formControlName="newsLetterEmail">
                       <input id="submit-btn" type="submit">
                     </form>
                   `,
        providers: [RegistrationForm],
      })
      class RegistrationComponent implements OnInit {

        constructor(public form: RegistrationForm) {
        }

        public ngOnInit(): void {
          this.form.init();
        }

      }

      let fixture: ComponentFixture<RegistrationComponent>;
      let component: RegistrationComponent;
      let debugElm: DebugElement;
      let submitInput: DebugElement;
      let usernameInput: DebugElement;
      let passwordInput: DebugElement;
      let emailInput: DebugElement;
      let newsLetter: DebugElement;
      let newsLetterEmail: DebugElement;

      beforeEach(A(async () => {
        await TestBed.configureTestingModule({
          declarations: [RegistrationComponent],
          imports:      [ReactiveFormsModule],
        }).compileComponents();

        fixture   = TestBed.createComponent(RegistrationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        debugElm        = fixture.debugElement;
        submitInput     = debugElm.query(By.css('#submit-btn'));
        usernameInput   = debugElm.query(By.css('input[name=username]'));
        passwordInput   = debugElm.query(By.css('input[name=password]'));
        emailInput      = debugElm.query(By.css('input[name=email]'));
        newsLetter      = debugElm.query(By.css('input[name=newsLetter]'));
        newsLetterEmail = debugElm.query(By.css('input[name=newsLetterEmail]'));

      }));

      it('Create', () => {

        expect(fixture).to.be.not.undefined;
        expect(component).to.be.not.undefined;
        expect(debugElm).to.be.not.undefined;
        expect(submitInput).to.be.not.undefined;
        expect(usernameInput).to.be.not.undefined;
        expect(passwordInput).to.be.not.undefined;
        expect(emailInput).to.be.not.undefined;
        expect(newsLetter).to.be.not.undefined;
        expect(newsLetterEmail).to.be.not.undefined;
        expect(submitInput).to.be.not.null;
        expect(usernameInput).to.be.not.null;
        expect(passwordInput).to.be.not.null;
        expect(emailInput).to.be.not.null;
        expect(newsLetter).to.be.not.null;
        expect(newsLetterEmail).to.be.null;

      });

      it('Invalid Submit', () => {

        submitInput.triggerEventHandler('click', null);

        expect(component.form.invalid).to.be.true;

      });

      it('Valid without news letter email', (() => {

        const username = 'username';
        const password = 'password';
        const email    = 'mail@mail.de';

        usernameInput.nativeElement.value = username;
        passwordInput.nativeElement.value = password;
        emailInput.nativeElement.value    = email;

        usernameInput.nativeElement.dispatchEvent(new Event('input'));
        passwordInput.nativeElement.dispatchEvent(new Event('input'));
        emailInput.nativeElement.dispatchEvent(new Event('input'));

        fixture.detectChanges();

        expect(component.form.username.value).to.be.eq(username);
        expect(component.form.password.value).to.be.eq(password);
        expect(component.form.email.value).to.be.eq(email);

        submitInput.triggerEventHandler('click', null);

        expect(component.form.valid).to.be.true;

      }));

      it('news letter checkbox', () => {

        component.form.newsLetter.setValue(true);
        fixture.detectChanges();
        newsLetterEmail = debugElm.query(By.css('input[name=newsLetterEmail]'));
        expect(newsLetterEmail).to.be.not.null;
        component.form.newsLetter.setValue(false);
        fixture.detectChanges();
        newsLetterEmail = debugElm.query(By.css('input[name=newsLetterEmail]'));
        expect(newsLetterEmail).to.be.null;

      });

      it('Invalid with news letter checkbox', () => {

        const username = 'username';
        const password = 'password';
        const email    = 'mail@mail.de';

        usernameInput.nativeElement.value = username;
        passwordInput.nativeElement.value = password;
        emailInput.nativeElement.value    = email;

        usernameInput.nativeElement.dispatchEvent(new Event('input'));
        passwordInput.nativeElement.dispatchEvent(new Event('input'));
        emailInput.nativeElement.dispatchEvent(new Event('input'));

        component.form.newsLetter.setValue(true);
        fixture.detectChanges();
        submitInput.triggerEventHandler('click', null);

        expect(component.form.invalid).to.be.true;

      });

      it('Valid with news letter email', () => {

        component.form.newsLetter.setValue(true);
        fixture.detectChanges();

        newsLetterEmail = debugElm.query(By.css('input[name=newsLetterEmail]'));

        const username = 'username';
        const password = 'password';
        const email    = 'mail@mail.de';

        usernameInput.nativeElement.value   = username;
        passwordInput.nativeElement.value   = password;
        emailInput.nativeElement.value      = email;
        newsLetterEmail.nativeElement.value = email;

        newsLetterEmail.nativeElement.dispatchEvent(new Event('input'));
        usernameInput.nativeElement.dispatchEvent(new Event('input'));
        passwordInput.nativeElement.dispatchEvent(new Event('input'));
        emailInput.nativeElement.dispatchEvent(new Event('input'));

        fixture.detectChanges();

        expect(component.form.newsLetterEmail.value).to.be.eq(email);

        submitInput.triggerEventHandler('click', null);

        expect(component.form.valid).to.be.true;

      });

      it('Valid without news letter email', () => {

        component.form.newsLetter.setValue(true);
        fixture.detectChanges();

        newsLetterEmail = debugElm.query(By.css('input[name=newsLetterEmail]'));

        const username = 'username';
        const password = 'password';
        const email    = 'mail@mail.de';

        usernameInput.nativeElement.value   = username;
        passwordInput.nativeElement.value   = password;
        emailInput.nativeElement.value      = email;
        newsLetterEmail.nativeElement.value = ''; // set invalid email

        newsLetterEmail.nativeElement.dispatchEvent(new Event('input'));
        usernameInput.nativeElement.dispatchEvent(new Event('input'));
        passwordInput.nativeElement.dispatchEvent(new Event('input'));
        emailInput.nativeElement.dispatchEvent(new Event('input'));

        fixture.detectChanges();

        submitInput.triggerEventHandler('click', null);

        expect(component.form.invalid).to.be.true;

      });

    });

  });

});
