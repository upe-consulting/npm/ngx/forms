import { Injectable } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { GenericFormControl, GenericFormGroup } from '../../lib/generic-form';
import { Control, Group } from '../../lib/generic-form/decoretors';

describe('Generic Form', () => {

  describe('Form Group', () => {

    @Injectable()
    @Group()
    class Form extends GenericFormGroup<any> {

      @Control()
      public input: GenericFormControl<any> = undefined as any;

    }

    let form: Form;

    beforeEach(() => {
      TestBed.configureTestingModule({ providers: [Form] });
      form = TestBed.get(Form);
      form.init();
    });


  });

});
