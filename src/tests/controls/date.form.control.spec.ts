import { SetDefaultDateFormat } from '@upe/ngx-form-validators';
import { expect } from 'chai';
import { DateFormControl } from '../../lib/controls';

describe('Generic Form', () => {

  describe('Controls', () => {

    describe('Date', () => {

      beforeEach(() => {
        SetDefaultDateFormat('DD.MM.YYYY');
      });

      xit('setModelValue', () => {

        const form = new DateFormControl(null);

        form.setModelValue('2018-04-02T00:00:00.000Z');

        expect(form.value).to.eq('02.04.2018');

      });

      xit('getModelValue', () => {

        const form = new DateFormControl(null);
        form.setValue('02.04.2018');

        expect(form.getModelValue()).to.eq('2018-04-02T00:00:00.000Z');

      });

    });

  });

});
